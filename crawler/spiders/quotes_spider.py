import scrapy
import logging
import time
import telegram_handler as tg
#import datetime
#import urllib.parse as urlparse
import requests
#import geocoder
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pyvirtualdisplay import Display
from selenium.webdriver.common.keys import Keys
from hashlib import sha256
#from fake_useragent import UserAgent
#import random
#import traceback
import json
import sys
import linecache


class job_tracker(scrapy.Spider):           
    name = "job_tracker"
    file_data = []
    current_data = []
    create_data = {}
    counter = 0
    requests_header = {'host': 'example.com'}
    logging.getLogger("telegram").propagate = False
    logging.getLogger('scrapy').propagate = False
    logging.getLogger('selenium').propagate = False
    with open('credentials.json', 'r') as credential_file:
        credentials_arr = json.load(credential_file)
        username = credentials_arr['username']
        password = credentials_arr['password']
        chat_id = credentials_arr['telegram_chat_id']
        job_category = credentials_arr['job_category']
    def phantom_settings_function(self):
        # PhantomJS settings
        phantom_settings = dict(DesiredCapabilities.PHANTOMJS)
        phantom_settings['phantomjs.page.settings.userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['userAgent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['User-Agent'] = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
        phantom_settings['phantomjs.page.settings.javascriptEnabled'] = True,   
        phantom_settings['phantomjs.page.settings.loadImages'] = True,
        phantom_settings['browserName'] = 'Google Chrome'
        phantom_settings['platformName'] = 'Linux'
        return phantom_settings
        
    def init_phantomjs_driver(self, *args, **kwargs):
    
        headers = { 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language':'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0',
            'Connection': 'keep-alive'
        }
    
        for key, value in enumerate(headers):
            webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.customHeaders.{}'.format(key)] = value
    
        webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
    
        driver =  webdriver.PhantomJS(*args, **kwargs)
        driver.set_window_size(1400,1000)
        print(webdriver.DesiredCapabilities.PHANTOMJS)
        return driver
        
    def __init__(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        self.options = Options()
        self.options.add_argument("headless")
        self.options.add_argument('--disable-gpu')
        self.options.headless = True
        self.options.set_headless(headless=True)
        self.options.add_argument("headless") # Runs Chrome in headless mode.
        self.options.add_argument('--no-sandbox') # Bypass OS security model
        self.options.add_argument('start-maximized') # 
        self.options.add_argument('disable-infobars')
        self.options.add_argument("--disable-extensions")
        self.headers = {
            'Accept':'*/*',
            'Accept-Encoding':'gzip, deflate, sdch',
            'Accept-Language':'en-US,en;q=0.8',
            'Cache-Control':'max-age=0',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
        }
        for key, value in enumerate(self.headers):
            capability_key = 'phantomjs.page.customHeaders.{}'.format(key)
            webdriver.DesiredCapabilities.PHANTOMJS[capability_key] = value
        self.driver = webdriver.Chrome(chrome_options=self.options)#(desired_capabilities=self.phantom_settings_function(),service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])
        #self.driver = self.init_phantomjs_driver()
        #self.driver = webdriver.PhantomJS(desired_capabilities =self.phantom_settings_function(), service_args=['--ignore-ssl-errors=true', '--ssl-protocol=tlsv1'])

    def stale_aware_for_action(self, action):
        while(True):
            try:
                action()
                break
            except StaleElementReferenceException:
                continue
            
    def start_requests(self):
        urls = [
            'https://ssoauth.jbhunt.com/cas-server/login?service=http%3A%2F%2Fdriverrecruiters.jbhunt.com%2Fc%2Fportal%2Flogin%3Fredirect%3D%252fhome%26p_l_id%3D0'
        ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, priority = 10)
    
    def PrintException(self):
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        error_message = 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
        print(error_message)
        return error_message
        
    
    
    def if_exist_in_file(self, Hex_Id):
        data = {"Hex_Id": Hex_Id}
        resp = json.loads(requests.post('http://157.230.185.129:3000/Record/ByHex',json=data, headers=self.requests_header).text)
        if (len(resp) > 0):
            return True
        return False
        
    def update_file(self, content=""):
        secret_data = {"Secret": "jb_hunt_upwork"}
        resp = requests.post('http://157.230.185.129:3000/Record/delete/', json=secret_data, headers=self.requests_header)
        time.sleep(2)
        for data in self.current_data:
            print("\n\nIN UPDATE FILE")
            print("\n\nDATA : \n")
            print(json.dumps(data))
            resp = requests.post('http://157.230.185.129:3000/Record', json=data, headers=self.requests_header)
            print("\n\nReponse : \n")
            print(resp.text)        
        tg.bot.send_message(chat_id="300781025", text="Yeni Datalar Kaydedildi.")
        
        
    def create_job_array_from_existing_jobs(self):
        self.file_data = json.loads(requests.get('http://157.230.185.129:3000/Record', headers=self.requests_header).text)

    def delete_spaces_from_string(self, string):
        return string.replace('\n','').replace('\t','').replace(' ','')
    
    
    def parse(self, response):
        try:
            self.driver.get(response.url)
            self.create_job_array_from_existing_jobs()
            #tg.bot.send_message(chat_id="300781025", text="Starting Bot.")
            time.sleep(10)
            username_field = self.driver.find_element_by_xpath('//*[@id="username"]')
            username_field.send_keys(self.username)
            password_field = self.driver.find_element_by_xpath('//*[@id="password"]')
            password_field.send_keys(self.password)
            password_field.send_keys(Keys.ENTER)
            time.sleep(10)
            career_path_select_box = self.driver.find_element_by_xpath('//*[@id="_app_jobsPortlet_WAR_app_jobsPortlet_:jobSearch:j_id22"]')
            select_element_career_path = Select(career_path_select_box)
            select_element_career_path.select_by_visible_text(self.job_category)
            self.driver.find_element_by_xpath('//*[@id="_app_jobsPortlet_WAR_app_jobsPortlet_:jobSearch:searchButton"]').click() # submit form
            time.sleep(3)
            job_table = self.driver.find_elements_by_xpath('//div[@id="_app_jobsPortlet_WAR_app_jobsPortlet_"]/form/span/table/tbody/tr')
            job_body = self.driver.find_element_by_tag_name('body')
            element_png = job_body.screenshot_as_png
            with open(str(int(time.time())) +".png", "wb") as file:
                file.write(element_png)
            for i in range(len(job_table)):
                job_table = self.driver.find_elements_by_xpath('//div[@id="_app_jobsPortlet_WAR_app_jobsPortlet_"]/form/span/table/tbody/tr')
                job_info_to_send = job_table[i].get_attribute('textContent').replace('\t', '')
                
                job_type = job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[1]/span').get_attribute('textContent')
                pos_tier = job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[2]/span').get_attribute('textContent')
                location = job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[3]/span').get_attribute('textContent')
                business_tier = job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[4]/span').get_attribute('textContent')
                exp_req = job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[5]/span').get_attribute('textContent')
                license_type = self.delete_spaces_from_string(job_table[i].find_element_by_xpath('./td/div[@class="row"]/a/div[6]').get_attribute('textContent'))
                self.create_data = {
                        "Job_Type" : job_type,
                        "Position_Tier" : pos_tier,
                        "Location" : location,
                        "Business_Unit" : business_tier,
                        "Experience_Required" : exp_req,
                        "License_Type" : license_type
                        }
                self.stale_aware_for_action(job_table[i].click)
                time.sleep(2)
                job_id = self.driver.find_element_by_xpath('//span[@class="indented"]').get_attribute('textContent')
                
                job_info = 'job_type: {}\npos_tier: {}\nlocation: {}\nbusiness_tier: {}\nexp_req: {}\nlicense_type: {}\nJob Id: {}'.format(job_type,pos_tier,location,business_tier,exp_req,license_type,job_id)

                Hex_Id = sha256(self.delete_spaces_from_string(job_info).encode()).hexdigest()
                
                self.create_data['Hex_Id'] = Hex_Id
                self.create_data['Job_Id'] = job_id
                print('\n'*5)
                print(self.create_data)
                print('\n'*5)
                self.current_data.append(self.create_data)
                self.create_data = {}
                if (self.if_exist_in_file(Hex_Id)):
                    print('{} already checked before.'.format(Hex_Id))
                else:
                    print("A new entry found --> {}. **Notification Sending**".format(Hex_Id))
                    #tg.bot.send_message(chat_id=self.chat_id, text="A new entry appears in the job list. \n {} ".format(job_info_to_send))
                    #tg.bot.send_message(chat_id="300781025", text="A new entry appears in the job list. \n {} ".format(job_info_to_send))
                self.stale_aware_for_action(self.driver.find_element_by_xpath('//a[@class="iceCmdLnk  indented"]').click)
                time.sleep(2)
                job_table = self.driver.find_elements_by_xpath('//div[@id="_app_jobsPortlet_WAR_app_jobsPortlet_"]/form/span/table/tbody/tr')
            
            self.update_file()
            #tg.bot.send_message(chat_id="300781025", text="Stopping Bot.")
        except:
            #tg.bot.send_message(chat_id="300781025", text=self.PrintException())
            pass

        finally:
            self.driver.close()
            self.display.stop()
            self.driver.quit()
